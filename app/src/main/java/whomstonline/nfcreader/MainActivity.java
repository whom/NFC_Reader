package whomstonline.nfcreader;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // ui elements
    TextView hexConsole = null;
    TextView textConsole = null;
    TextView idView = null;

    // foreground dispatch items
    PendingIntent pendingIntent = null;
    IntentFilter[] intentFilterArray = null;
    String[][] techListsArray = null;

    // nfc stuff
    NfcAdapter nfcAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get text buffers
        hexConsole = (TextView) findViewById(R.id.hexConsole);
        textConsole = (TextView) findViewById(R.id.textConsole);
        idView = (TextView) findViewById(R.id.idDisplay);

        // print default message
        printToConsoles(new String(new char[200]).replace("\0", "0"));

        // set up foreground dispatch
        pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this,
                       getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // intent filters
        IntentFilter tag = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        intentFilterArray = new IntentFilter[] {tag,};
        techListsArray = new String[][] {new String[] {NfcF.class.getName()},
                new String[] {NfcA.class.getName()},
                new String[] {NfcB.class.getName()},
                new String[] {NfcV.class.getName()},
                new String[] {IsoDep.class.getName()},
                new String[] {MifareClassic.class.getName()},
                new String[] {MifareUltralight.class.getName()}
        };

        // get Nfc Adapter
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilterArray, techListsArray);
    }

    public void onNewIntent(Intent intent) {
        // an intent has been dispatched to us
        Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        // parse id
        if(tagFromIntent.getId() != null) {
            String id = "";

            // convert to hex
            for (int i : tagFromIntent.getId()) {
                id += Integer.toHexString(i);
            }

            // display id
            idView.setText("HEX ID: " + id);

        // i guess it happens...
        } else {
            idView.setText("NO ID");
        }

        // was going to let typed nulls be typed null but there were errors so...
        String text = "null";
        // pull data from tag depending on what tech it implements.
        techloop:
        for (String tech : tagFromIntent.getTechList()){
            Log.d("WHOMST--->", tech);

            techswitch:
            switch (tech) {

                // TODO: prompt user for input.
                case "android.nfc.tech.IsoDep":
                    IsoDep i = IsoDep.get(tagFromIntent);
                    Toast.makeText(this, "Connecting to IsoDep", Toast.LENGTH_LONG);

                    try {
                        i.connect();

                        // one of the two functions will return null
                        byte[] b = i.getHiLayerResponse();
                        byte[] c = i.getHistoricalBytes();

                        try{
                            text = new String(b);

                        } catch( NullPointerException e ){
                            text = new String(c);
                        }

                        i.close();

                    } catch (Exception e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    }

                    break techloop;

                // TODO: prompt user for key for auth, perhaps try a brute force, the key is 128 bits
                case "android.nfc.tech.MifareClassic":
                    Toast.makeText(this, "MifareClassic requires auth before IO", Toast.LENGTH_LONG);
                    break techloop;

                // TODO: prompt user for key for auth, perhaps try a brute force
                case "android.nfc.tech.MifareUltralight":
                    MifareUltralight k = MifareUltralight.get(tagFromIntent);
                    Toast.makeText(this, "Connecting to Mifare Ultralight", Toast.LENGTH_LONG);

                    // info about Mifare UL
                    int pages = 16;
                    int pagesize = 4;

                    // ultralight type c is 192 bytes with 4 nonreadable pages for auth key
                    if (k.getType() == 2) {
                        pages = 44;
                    }

                    try {
                        k.connect();

                        for (int iter = 0; iter < pages; iter++) {
                            text += k.readPages(iter * pagesize).toString();
                        }

                        k.close();

                    } catch (Exception e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
                        Log.d("WHOMST--->", e.getMessage());
                    }
                    break techloop;

                // TODO: prompt user commands to the nfcv
                case "android.nfc.tech.NfcV":
                    NfcV l = NfcV.get(tagFromIntent);
                    Toast.makeText(this, "Connecting to NfcV", Toast.LENGTH_LONG);

                    try {
                        l.connect();
                        text += l.getDsfId();
                        l.close();

                    } catch (Exception e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
                        Log.d("WHOMST--->", e.getMessage());
                    }
                    break techloop;

                // TODO: prompt user commands to the nfcf
                case "android.nfc.tech.NfcF":
                    NfcF m = NfcF.get(tagFromIntent);
                    Toast.makeText(this, "Connecting to NfcF", Toast.LENGTH_LONG);

                    try {
                        m.connect();
                        text += m.getManufacturer();
                        text += m.getSystemCode();
                        m.close();

                    } catch (Exception e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
                        Log.d("WHOMST--->", e.getMessage());
                    }

                    break techloop;

                // TODO: same command stuff as before
                case "android.nfc.tech.NfcB":
                    NfcB n = NfcB.get(tagFromIntent);
                    Toast.makeText(this, "Connecting to NfcB", Toast.LENGTH_LONG);

                    try {
                        n.connect();
                        text += n.getProtocolInfo();
                        text += n.getApplicationData();
                        n.close();

                    } catch (Exception e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
                        Log.d("WHOMST--->", e.getMessage());
                    }

                    break techloop;

                // TODO: same command stuff
                case "android.nfc.tech.NfcA":
                    NfcA o = NfcA.get(tagFromIntent);
                    Toast.makeText(this, "Connecting to NfcA", Toast.LENGTH_LONG);

                    try {
                        o.connect();
                        text += o.getAtqa().toString();
                        o.close();

                    } catch (Exception e) {
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
                        Log.d("WHOMST--->", e.getMessage());
                    }

                    break techloop;
            }
        }

        // fucking typed nulls, man.
        if(text.equals("null")){
            Toast.makeText(this,"No understood techs found in tag. Printing techs in tag.", Toast.LENGTH_LONG);
            Log.d("WHOMST--->", "Text was not found");

            for(String t : tagFromIntent.getTechList()){
                text += t + ", ";
            }
        }

        // send text to buffer
        printToConsoles(text);
    }

    private void printToConsoles(String text){
        // clear buffers
        hexConsole.setText("");
        textConsole.setText("");

        for(char c : text.toCharArray()){
            textConsole.append(c + " ");
            hexConsole.append( Integer.toHexString((int) c).toUpperCase() + " ");
        }
    }
}
